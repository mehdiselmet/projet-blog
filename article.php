<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
   <link rel="stylesheet" href="./assets/css/style.css">
   <?php include("Views/header.php"); ?>
</head>
<body>
    
</body>
</html>
<?php
require_once('database.php');

$article_id = null ;

if (!empty($_GET['id']) && ctype_digit($_GET['id'])) {
    $article_id= $_GET['id'] ;
}

if (!$article_id) {
    die("id inexistant dans url !") ;
}

$resultat = $pdo->prepare("SELECT * FROM articles WHERE id = :article_id");
$resultat->execute(['article_id' => $article_id]);

$article = $resultat->fetch();
?>
<!-- Page Wrapper -->
<div class="page-wrapper">

  <!-- Content -->
  <div class="content clearfix">

   <!-- Main Content Wrapper -->
   <div class="main-content-wrapper">
        <div class="main-content single">
<?php
echo "<h1>" . $article['title'] . "</h1><br>";
echo $article['content'] . "<br>";
?>



<?php
// afficher les commentaires
$query= $pdo->prepare("SELECT* FROM comments WHERE article_id = :article_id");
$query->execute(['article_id' => $article_id]);
$comments = $query->fetchAll();
?>
    <!-- 4. On boucle sur la table articles[] pour afficher chaque article -->

<h1>commentaires</h1>
<form action="">

<textarea name="textarea" rows="10" cols="50" placeholder="laisser un message"></textarea><br>

<input type="submit"  name="envoyer"><br>
</form>

    <?php
    foreach ($comments as $comment) {
        echo "<h3>" . $comment['author'] . "</h3>" ;
        echo "<small>Ecrit le" . $comment['created_at'] . "</small>";
        echo "<p>" . $comment['content'] . "</p> " ;
    }
?> 
 </div>

</div>
</div>