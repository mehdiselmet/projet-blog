<?php
 
// 1. On se connecte à la base de données avec PDO
 
require_once('database.php') ;
// require_once('function.php') ;

// 2. On récupère les articles du blog
 
// On utilise ici la méthode query 
// pas besoin de préparation ici car aucune variable n'est utilisée
$resultats = $pdo->query('SELECT * FROM articles ORDER BY created_at DESC LIMIT 3');
 
// On extrait les données que l'on met dans $articles
$articles = $resultats->fetchAll(); // Retourne le resultat dans le tableau $article[]
 
?>
 
<!-- 3. on affiche le titre de la page -->
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CBG</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
    integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    
    <!-- Style css -->
    <link rel="stylesheet" href="./assets/css/style.css">
    <?php include("Views/header.php"); ?>
</head>
<body>
    <!-- NAVIGATION -->
  
    <!-- <div class="container"> -->
        <!-- BANDEAU -->
        <!-- <img class="bandeau" src="./img/headercbg.jpg" title="headercbg"> -->
        
        <!-- CAROUSSEL -->
        
        <h1 class="caroussel-header">Les articles populaires</h1>  
 
        <div class="caroussel">
            
            <div class="cards">
                        
            <?php
                // 4. On boucle sur la table articles[] pour afficher les 3 derniers articles
                foreach ($articles as $article) {
                    echo "<div class='article'>" ;
                    echo    "<img src='./img/headercbg.jpg' class='image'>";
                    echo    "<div class='article-title'>" ;
                    echo        "<h3><a href='article.php?id=" . $article['id'] . "'>" . $article['title'] . "</a></h3>" ;
                    echo        "<p>Ecrit le " . $article['created_at'] . "</p>";
                    echo        "<p>Auteur " . $article['author_id'] . "</p>" ;
                    echo    "</div>";
                    echo "</div>";
                    // echo     "<div class='article-body><p>" . $article['introduction'] . "</p></div>" ;
                    // echo     "<div class='article-footer'>" ;
                    // echo     "<span>12 j'aime</span>";
                    // echo     "<span>15 commentaires</span>";
                    // echo     "<a href='article.php?id=" . $article['id'] . "'> Lire la suite </a>";
                    // echo     "</div>" ;   
                }
            ?>
        </div>
    </div>
 
    <!-- CONTENU -->


 
</body>
</html>