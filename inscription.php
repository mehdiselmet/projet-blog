<?php
require_once('database.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./assets/css/style.css">
    <?php 
    include("Views/header.php"); 
    ?>
 
</head>
<body>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Realtime Chat App | Aurige</title>
    <link rel="stylesheet" href="./assets/css/styleconnexion.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"/>
</head>
<body>
    <div class="wrapper">
        <section class="form signup">
            <header class="header">Cité Blanche Gutenberg</header>
            <form action="#">
                <div class="name-details">
                    <div class="field input">
                        <label>prénom</label>
                        <input type="text" placeholder="fname">
                    </div>
                    <div class="field input">
                        <label>nom de famille</label>
                        <input type="text" placeholder="nom de famiile" name="lname">
                    </div>
                </div>
                <div class="field input">
                    <label>Email </label>
                    <input type="text" placeholder="email" name="email">
                </div>
                <div class="field input">
                    <label>pseudo</label>
                    <input type="text" placeholder="pseudo" name="uname">
                    <i class="fas fa-eye" ></i>
                </div>
                <div class="field input">
                    <label>Password</label>
                    <input type="password" placeholder="Enter new password" name="pass">
                    <i class="fas fa-eye" ></i>
                </div>
                <!-- <div class="field image">
                    <label>Select Avatar</label>
                    <input type="file">
                </div> -->
                <div class="field button">
                    <input type="submit" name="Envoyer">
                </div>
            </form>
            <div class="link">Déjà inscrit ? <a href="login.php">Connecte toi !</a></div>
        </section>
    </div>
    
</body>
</html>
 
<?php
 
if(isset($_POST['Envoyer'])) {
 
    $uname = $_POST['uname'] ;
    $query = $pdo->prepare("SELECT count(*) FROM users WHERE username = ?") ;
    $query->execute([$uname]); 
    $count = $query->fetchColumn();
 
    if($count==0) {
        /* préparation de la requête d'insertion */
        $query= $pdo->prepare("INSERT INTO users VALUES(NULL, :fname, :lname, :email, :uname, :pass, NULL, NULL, NULL, NULL)");
 
        /* liaison de chaque champ de la table users à un input du formulaire */
        $query->bindValue(':fname', $_POST['first_name'], PDO::PARAM_STR) ;
        $query->bindValue(':lname', $_POST['last_name'], PDO::PARAM_STR) ;
        $query->bindValue(':email', $_POST['email'], PDO::PARAM_STR) ;
        $query->bindValue(':uname', $_POST['username'], PDO::PARAM_STR) ;
        $query->bindValue(':pass', $_POST['password'], PDO::PARAM_STR) ;
    
        /* execution de la requête préparée*/
        $insertOk = $query->execute();
    
        if($insertOk) {
            header('Location: index.php') ;
        } else {
            echo "Echec d'insertion" ;
        }
    } else {
        echo "Le username existe déjà" ;
    }
}
    
?>