<?php 
    session_start();
    require_once 'database.php';
    include("Views/header.php");
?>
 
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Realtime Chat App | Aurige</title>
    <link rel="stylesheet" href="./assets/css/styleconnexion.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"/>
       
</head>
<body>
    <div class="wrapper">
        <section class="form login">
            <header class="header">Cité Blanche Gutenberg</header>
            <form action="#">
                <div class="field input">
                    <label>Pseudo</label>
                    <input type="text" placeholder="Votre pseudo" name='username'>
                </div>
                <div class="field input">
                    <label>Password</label>
                    <input type="text" placeholder="Entre votre mot de passe" name="password">
                    <i class="fas fa-eye" ></i>
                </div>
 
                <div class="field button">
                    <input type="submit" value="Continue to Chat">
                </div>
            </form>
            <div class="link">Pas encore inscrit ? <a href="inscription.php">Inscris toi !</a>
            
            </div>
        </section>
    </div>
    
</body>
</html>
<?php
    if(isset($_POST['connexion'])) {
        if(!empty($_POST['username']) && !empty($_POST['password'])) {
            // assure toi que tu as bien mis username et password dans les input name
            $uname = htmlspecialchars($_POST['username']);
            $password = htmlspecialchars($_POST['password']);
 
            $check = $pdo->prepare('SELECT username, password FROM users WHERE username = ?');
            $check->execute(array($uname));
            $data = $check->fetch();
            $row = $check->rowCount();
            $pass = $data['password'] ;
 
            if($row == 1) {
                if($pass==$password) {
                    $_SESSION['user'] = $data['username'];
                    header('Location: index.php');
                    die();
                } else { header('Location: login.php?login_err=password'); die(); }
            } else { header('Location: login.php?login_err=notfound'); die(); }
        }
    }
    ?>